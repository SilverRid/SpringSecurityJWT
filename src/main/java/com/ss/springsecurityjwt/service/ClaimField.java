package com.ss.springsecurityjwt.service;

/**
 * @author Sergei Iurochkin
 */
public interface ClaimField {
	String USERNAME = "username";
	String ROLE = "role";
	String USER_ID = "user_id";
	String CONFIRMATION = "confirmation";
}
