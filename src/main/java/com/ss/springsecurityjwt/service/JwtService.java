package com.ss.springsecurityjwt.service;

import com.ss.springsecurityjwt.model.Jwt;
import io.jsonwebtoken.Claims;
import org.springframework.security.core.Authentication;

/**
 * @author Sergei Iurochkin
 */
public interface JwtService {
	String generatedJwt(Authentication authentication);
	Claims getClaims(String jwt);
	boolean isValidJwt(Jwt jwt);
}