package com.ss.springsecurityjwt.service;

/**
 * @author Sergei Iurochkin
 */
public interface HeaderValues {
	String AUTHORIZATION = "Authorization";
	String BEARER = "Bearer ";
}
